PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man

# Compiler options
CC=gcc
LD=gcc
CFLAGS=-std=c99 -D_POSIX_C_SOURCE=200809L
LDFLAGS=

# Our files

CFILES=$(wildcard *.c)
HFILES=$(wildcard *.h)
OFILES=$(patsubst %.c,%.o,$(CFILES))

default: all
.PHONY: all
all: matroid-finder

matroid-finder: $(OFILES)
	$(LD) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	rm -f $(OFILES)
	rm -f matroid-finder

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f matroid-finder $(DESTDIR)$(BINDIR)
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f matroid-finder.1 $(DESTDIR)$(MANDIR)/man1/matroid-finder.1

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f matroid-finder
	cd $(DESTDIR)$(MANDIR)/man1 && rm -f matroid-finder.1

# Not automatically kept in sync!

matroid-finder.o: matroid-finder.c Makefile $(HFILES)
	$(CC) $(CFLAGS) -o $@ -c $<
