#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <locale.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wchar.h>
#include <wctype.h>

#define STR2(x) #x
#define STR(x) STR2(x)

#define L(x) __FILE__ ":" STR(__LINE__) ": " x
#define UNUSED(x) (void) (sizeof(x))

#define BOUNDARY_NODE ((node *) 0)
#define NODE_NAME(n) ((n) ? ((n)->name) : ("BOUNDARY"))
#define NODE_COLOR(n) ((n) ? ((n)->c) : (C_BOUNDARY))

typedef enum { C_BLACK, C_WHITE, C_BOUNDARY } color;
typedef enum { D_LEFT, D_RIGHT } dir;
typedef enum { R_AWAY, R_TO } relative_dir;
typedef struct {
        /* */
        char *name;
        size_t name_len;
        color c;
        char check_res;
} node;
typedef struct {
        /* */
        node *l;
        node *r;
        dir d;
        unsigned int fix_depth;
} edge;
typedef struct {
        /* */
        size_t nodes_num;
        node *nodes;
        size_t edges_num;
        edge *edges;
} graph;

static int
add_new_node(graph *g,
             color c,
             const char *s,
             size_t n)
{
        void *newmem = 0;
        char *sd;
        int ret = -1;
        size_t j = 0;
        const char *p;

        if (n > INT_MAX / 2) {
                fprintf(stderr, "Absurdly long node name\n");
                goto cleanup;
        }

        if (!strncmp(s, "BOUNDARY", n) &&
            n == 8) {
                fprintf(stderr, "The node name `BOUNDARY' is forbidden\n");
                goto cleanup;
        }

        for (p = s; p < s + n; ++p) {
                if (*p == '[' ||
                    *p == ']' ||
                    *p == '(' ||
                    *p == ')') {
                        fprintf(stderr,
                                "Node names are not allowed to contain `[', "
                                "`]', `(', or `)'");
                        goto cleanup;
                }
        }

        for (j = 0; j < g->nodes_num; ++j) {
                if (!strncmp(s, g->nodes[j].name, n) &&
                    g->nodes[j].name_len == n) {
                        fprintf(stderr, "Duplicate node %s\n",
                                g->nodes[j].name);
                        goto cleanup;
                }
        }

        if (!(newmem = realloc(g->nodes, (g->nodes_num + 1) *
                               sizeof *g->nodes))) {
                perror(L("realloc"));
                goto cleanup;
        }

        if (!(sd = strndup(s, n + 1))) {
                perror(L("strndup"));
                goto cleanup;
        }

        sd[n] = '\0';
        g->nodes = newmem;
        g->nodes[g->nodes_num] = (node) { .name = sd, .name_len = n, .c = c };
        newmem = sd = 0;
        g->nodes_num++;
        ret = 0;
cleanup:
        free(newmem);
        free(sd);

        return ret;
}

static int
add_new_edge(graph *g,
             const char *s1,
             size_t n1,
             const char *s2,
             size_t n2)
{
        void *newmem = 0;
        edge *e = 0;
        node *v1 = 0;
        node *v2 = 0;
        int ret = -1;
        size_t j = 0;

        if (n1 > INT_MAX / 2 ||
            n2 > INT_MAX / 2) {
                fprintf(stderr, "Absurdly long node name\n");
                goto cleanup;
        }

        for (j = 0; j < g->nodes_num; ++j) {
                if (!strncmp(s1, g->nodes[j].name, n1) &&
                    g->nodes[j].name_len == n1) {
                        v1 = &g->nodes[j];
                        break;
                }
        }

        for (j = 0; j < g->nodes_num; ++j) {
                if (!strncmp(s2, g->nodes[j].name, n2) &&
                    g->nodes[j].name_len == n2) {
                        v2 = &g->nodes[j];
                        break;
                }
        }

        if (!v1) {
                if (strncmp(s1, "BOUNDARY", n1) ||
                    n1 != 8) {
                        fprintf(stderr, "Unknown node `%.*s'\n", (int) n1, s1);
                        goto cleanup;
                }
        }

        if (!v2) {
                if (strncmp(s2, "BOUNDARY", n2) ||
                    n2 != 8) {
                        fprintf(stderr, "Unknown node `%.*s'\n", (int) n2, s2);
                        goto cleanup;
                }
        }

        if (v1 == v2) {
                fprintf(stderr, "Illegal node (%s, %s)\n", NODE_NAME(v1),
                        NODE_NAME(v2));
                goto cleanup;
        }

        for (j = 0; j < g->edges_num; ++j) {
                e = &g->edges[j];

                if ((e->l == v1 &&
                     e->r == v2) ||
                    (e->l == v2 &&
                     e->r == v1)) {
                        fprintf(stderr, "Duplicate edge (%s, %s)\n", NODE_NAME(
                                        e->l), NODE_NAME(e->r));
                        goto cleanup;
                }
        }

        if (!(newmem = realloc(g->edges, (g->edges_num + 1) *
                               sizeof *g->edges))) {
                perror(L("realloc"));
                goto cleanup;
        }

        g->edges = newmem;
        g->edges[g->edges_num] = (edge) { .l = v1, .r = v2, .fix_depth = -1 };
        newmem = 0;
        g->edges_num++;
        ret = 0;
cleanup:
        free(newmem);

        return ret;
}

/*
 * Order edges first by their left component, then by their right
 * component. This ensures that the edges representing boundary nodes
 * are always first.
 */
static int
comp_edge(const void *a,
          const void *b)
{
        edge *pa = (edge *) a;
        edge *pb = (edge *) b;
        int d = 0;

        if (!pa->l &&
            pb->l) {
                return -1;
        }

        if (pa->l &&
            !pb->l) {
                return 1;
        }

        if (pa->l &&
            pb->l) {
                d = strcmp(pa->l->name, pb->l->name);

                if (d) {
                        return d;
                }
        }

        return strcmp(pa->r->name, pb->r->name);
}

/*
 * Returns the address of the first byte of a [non-]whitespace character
 * beyond s, or 0 on error, or terminus on exhaustion.
 */
static const char *
next_wchar_of_certain_type(const char *s,
                           const char *terminus,
                           char want_whitespace)
{
        const char *p = s;
        mbstate_t ps = { 0 };
        size_t e = 0;
        wchar_t wc = 0;

        if (s == terminus) {
                return 0;
        }

        while (p < terminus) {
                switch ((e = mbrtowc(&wc, p, (terminus - p) + 1, &ps))) {
                case (size_t) -1:
                case (size_t) -2:
                        perror(L("mbrtowc"));

                        return 0;
                        break;
                case 0:

                        return 0;
                        break;
                default:

                        if ((!!want_whitespace == !!iswspace(wc)) &&
                            p != s) {
                                return p;
                        }
                }

                p += e;
        }

        return terminus;
}

static int
read_file(const char *path,
          graph *g)
{
        int fd = 0;
        int ret = -1;
        char *contents = 0;
        const char *contents_end = 0;
        const char *p = 0;
        const char *q = 0;
        const char *r = 0;
        const char *s = 0;
        const char *t = 0;
        struct stat sb;
        size_t length = 0;

        if ((fd = open(path, O_RDONLY)) < 0) {
                perror(L("open"));
                goto cleanup;
        }

        if (fstat(fd, &sb) < 0) {
                perror(L("fstat"));
                goto cleanup;
        }

        length = sb.st_size;

        if (length < 4) {
                fprintf(stderr, "File is not well-formed\n");
                goto cleanup;
        }

        if ((contents = mmap(0, length, PROT_READ, MAP_PRIVATE, fd, 0)) ==
            MAP_FAILED) {
                perror("mmap");
                goto cleanup;
        }

        contents_end = contents + (length - 1);

        if (!(p = memchr(contents, '[', length)) ||
            strncmp(p, "[W]", 3)) {
                fprintf(stderr, "File does not contain `[W]' marker\n");
                goto cleanup;
        }

        q = p + 3;
        r = memchr(q, '[', contents_end - q);

        if (!r ||
            strncmp(r, "[B]", 3)) {
                fprintf(stderr, "File does not contain `[B]' marker\n");
                goto cleanup;
        }

        p = next_wchar_of_certain_type(q, r, 0);
        q = next_wchar_of_certain_type(p, r, 1);

        while (q &&
               q <= r) {
                if (p != q) {
                        if (add_new_node(g, C_WHITE, p, q - p) < 0) {
                                goto cleanup;
                        }
                }

                p = next_wchar_of_certain_type(q, contents_end, 0);
                q = next_wchar_of_certain_type(p, contents_end, 1);
        }

        p = r + 3;
        r = memchr(p, '[', contents_end - q);

        if (!r ||
            strncmp(r, "[E]", 3)) {
                fprintf(stderr, "File does not contain `[E]' marker\n");
                goto cleanup;
        }

        p = next_wchar_of_certain_type(p, r, 0);
        q = next_wchar_of_certain_type(p, r, 1);

        while (q &&
               q <= r) {
                if (p != q) {
                        if (add_new_node(g, C_BLACK, p, q - p) < 0) {
                                goto cleanup;
                        }
                }

                p = next_wchar_of_certain_type(q, contents_end, 0);
                q = next_wchar_of_certain_type(p, contents_end, 1);
        }

        s = r;

        while (1) {
                if (!(p = memchr(s, '(', contents_end - s))) {
                        break;
                }

                if (!(p = next_wchar_of_certain_type(p, contents_end, 0))) {
                        break;
                }

                if (!(q = next_wchar_of_certain_type(p, contents_end, 1))) {
                        break;
                }

                if (!(r = next_wchar_of_certain_type(q, contents_end, 0))) {
                        break;
                }

                if (!(s = next_wchar_of_certain_type(r, contents_end, 1))) {
                        break;
                }

                if (!(t = memchr(r, ')', contents_end - r))) {
                        break;
                }

                s = (s < t) ? s : t;

                if (add_new_edge(g, p, q - p, r, s - r) < 0) {
                        goto cleanup;
                }
        }

        ret = 0;
cleanup:

        if (fd) {
                close(fd);
        }

        if (contents) {
                munmap(contents, length);
        }

        return ret;
}

/*
 * Modify graph g so that all edges containing v point away from (to)
 * node v when away==1 (0), except the specified exception edge. If a
 * contradiction is encountered, *proven_wrong is set. If anything is
 * done, *changed is set.
 */
static void
ensure_all_pointing_right_dir(graph *g,
                              node *v,
                              relative_dir rd,
                              unsigned int depth,
                              edge *except,
                              char *changed,
                              char *proven_wrong)
{
        size_t j;
        edge *e;

        if (!g ||
            !v ||
            *proven_wrong) {
                return;
        }

        for (j = 0; j < g->edges_num; ++j) {
                if ((e = &g->edges[j]) == except) {
                        continue;
                }

                if (e->l == v) {
                        if (e->fix_depth <= depth &&
                            e->d == (rd == R_AWAY ? D_LEFT : D_RIGHT)) {
                                *proven_wrong = 1;

                                return;
                        }

                        if (e->fix_depth > depth) {
                                *changed = 1;
                                e->fix_depth = depth;
                                e->d = (rd == R_AWAY ? D_RIGHT : D_LEFT);
                        }
                } else if (e->r == v) {
                        if (e->fix_depth <= depth &&
                            e->d == (rd == R_AWAY ? D_RIGHT : D_LEFT)) {
                                *proven_wrong = 1;

                                return;
                        }

                        if (e->fix_depth > depth) {
                                *changed = 1;
                                e->fix_depth = depth;
                                e->d = (rd == R_AWAY ? D_LEFT : D_RIGHT);
                        }
                }
        }
}

/*
 * Check graph g to ensure that there is at least one edge containing
 * v which is able to be set to point away from (to) node v when
 * away==1 (0). If none is found, *proven_wrong is set. If exactly one
 * is found, it is fixed with the appropriate depth and *changed is
 * set.
 */
static void
ensure_one_left_to_point_right(graph *g,
                               node *v,
                               relative_dir rd,
                               unsigned int depth,
                               char *changed,
                               char *proven_wrong)
{
        size_t j;
        edge *e = 0;
        edge *candidate = 0;

        if (!g ||
            !v ||
            *proven_wrong) {
                return;
        }

        for (j = 0; j < g->edges_num; ++j) {
                e = &g->edges[j];

                if (e->fix_depth <= depth) {
                        if (!(e->l == v &&
                              e->d == (rd == R_AWAY ? D_RIGHT : D_LEFT)) &&
                            !(e->r == v &&
                              e->d == (rd == R_AWAY ? D_LEFT : D_RIGHT))) {
                                /*
                                 * Ignore fixed nodes that don't have
                                 * right orientation and endpoint
                                 */
                                continue;
                        }

                        /*
                         * We don't just have a candidate, we have an
                         * actual fixed edge. Don't bother checking
                         * for the error case of two edges, or
                         * forcibly setting all other edges to the
                         * right direction, those actions are handled
                         * in ensure_all_pointing_right_dir().
                         */
                        return;
                } else if (e->l == v ||
                           e->r == v) {
                        if (candidate) {

                                /*
                                 * There are multiple candidates, so
                                 * we can't set anything
                                 */
                                return;
                        }

                        candidate = e;
                }
        }

        if (!candidate) {
                /*
                 * There's nothing that could save us - this
                 * configuration is impossible.
                 */
                *proven_wrong = 1;

                return;
        }

        *changed = 1;
        candidate->fix_depth = depth;

        if ((candidate->l == v &&
             rd == R_AWAY) ||
            (candidate->r == v &&
             rd == R_TO)) {
                candidate->d = D_RIGHT;
        } else {
                candidate->d = D_LEFT;
        }
}

static void
update_for_newly_fixed_edge(graph *g,
                            edge *e,
                            unsigned int depth,
                            char *changed,
                            char *proven_wrong)
{
        if (!g ||
            !e) {
                return;
        }

        switch (e->d) {
        case D_LEFT:

                switch (NODE_COLOR(e->l)) {
                case C_WHITE:
                        ensure_all_pointing_right_dir(g, e->l, R_AWAY, depth, e,
                                                      changed, proven_wrong);
                        break;
                case C_BLACK:
                        ensure_one_left_to_point_right(g, e->l, R_AWAY, depth,
                                                       changed, proven_wrong);
                        break;
                default:
                        break;
                }

                if (*proven_wrong) {
                        return;
                }

                switch (NODE_COLOR(e->r)) {
                case C_WHITE:
                        ensure_one_left_to_point_right(g, e->r, R_TO, depth,
                                                       changed, proven_wrong);
                        break;
                case C_BLACK:
                        ensure_all_pointing_right_dir(g, e->r, R_TO, depth, e,
                                                      changed, proven_wrong);
                        break;
                default:
                        break;
                }

                break;
        case D_RIGHT:

                switch (NODE_COLOR(e->l)) {
                case C_WHITE:
                        ensure_one_left_to_point_right(g, e->l, R_TO, depth,
                                                       changed, proven_wrong);
                        break;
                case C_BLACK:
                        ensure_all_pointing_right_dir(g, e->l, R_TO, depth, e,
                                                      changed, proven_wrong);
                        break;
                default:
                        break;
                }

                if (*proven_wrong) {
                        return;
                }

                switch (NODE_COLOR(e->r)) {
                case C_WHITE:
                        ensure_all_pointing_right_dir(g, e->r, R_AWAY, depth, e,
                                                      changed, proven_wrong);
                        break;
                case C_BLACK:
                        ensure_one_left_to_point_right(g, e->r, 1, depth,
                                                       changed, proven_wrong);
                        break;
                default:
                        break;
                }

                break;
        }
}

/*
 * Repeatedly call update_for_newly_fixed_edge on all edges of a given
 * depth, possibly fixing more choices of edge directions at that
 * depth.
 */
static void
update_to_fixed_point(graph *g,
                      unsigned int depth,
                      char *broken,
                      char allow_scan_below_depth)
{
        char changed = 1;
        edge *e = 0;
        size_t j;

        while (changed &&
               !*broken) {
                changed = 0;

                for (j = 0; j < g->edges_num; ++j) {
                        e = &g->edges[j];

                        if (e->fix_depth == depth ||
                            (allow_scan_below_depth &&
                             e->fix_depth <= depth)) {
                                update_for_newly_fixed_edge(g, e, depth,
                                                            &changed, broken);
                        }
                }
        }
}

int
admits_perfect_orientation(graph *g,
                           unsigned int depth)
{
        edge *e;
        edge *chosen_to_fix = 0;
        char broken = 0;
        size_t j = 0;

        if (depth >= (unsigned int) -3) {
                fprintf(stderr, "Absurd depth encountered - bailing\n");

                return 0;
        }

        for (j = 0; j < g->edges_num; ++j) {
                e = &g->edges[j];

                if (e->fix_depth > depth) {
                        /*
                         * In the case that some edges were tested
                         * before for other configurations, we wish to
                         * avoid the situation in which such a branch
                         * was tested at depth depth+1, and is not
                         * selected as the unfixed branch here. For
                         * then it would, in any speculation arising
                         * from this position, magically appear to be
                         * fixed, when it really should not be.
                         */
                        e->fix_depth = (unsigned int) -1;
                        chosen_to_fix = e;
                }
        }

        if (chosen_to_fix) {
                chosen_to_fix->d = D_LEFT;

                /*
                 * We start out at depth + 2 so that, if this doesn't
                 * work out, the ramifications will be nicely hidden
                 * by moving to only depth + 1 in the next step
                 */
                chosen_to_fix->fix_depth = depth + 2;
                update_to_fixed_point(g, depth + 2, &broken, 0);

                if (!broken &&
                    admits_perfect_orientation(g, depth + 2)) {
                        return 1;
                }

                /*
                 * Evidently setting that direction to LEFT resulted
                 * in a contradiction. So we may freely set it to
                 * RIGHT. We could really set the depth to `depth'
                 * here, but that doesn't really gain anything.
                 */
                chosen_to_fix->d = D_RIGHT;
                chosen_to_fix->fix_depth = depth + 1;
                broken = 0;
                update_to_fixed_point(g, depth + 1, &broken, 0);

                if (!broken &&
                    admits_perfect_orientation(g, depth + 1)) {
                        return 1;
                }

                /*
                 * There was a choice we could make, and each choice
                 * led to a contradiction.
                 */
                return 0;
        }

        /*
         * At this point, there's nothing we may choose. Just check
         * whether our choices made sense.
         */
        for (j = 0; j < g->nodes_num; ++j) {
                g->nodes[j].check_res = 0;
        }

        for (j = 0; j < g->edges_num; ++j) {
                e = &g->edges[j];

                switch (e->d) {
                case D_LEFT:

                        if (e->l &&
                            e->l->c == C_WHITE) {
                                broken = broken ||
                                         e->l->check_res;
                                e->l->check_res = 1;
                        }

                        if (e->r &&
                            e->r->c == C_BLACK) {
                                broken = broken ||
                                         e->r->check_res;
                                e->r->check_res = 1;
                        }

                        break;
                case D_RIGHT:

                        if (e->l &&
                            e->l->c == C_BLACK) {
                                broken = broken ||
                                         e->l->check_res;
                                e->l->check_res = 1;
                        }

                        if (e->r &&
                            e->r->c == C_WHITE) {
                                broken = broken ||
                                         e->r->check_res;
                                e->r->check_res = 1;
                        }

                        break;
                }

                if (broken) {
                        break;
                }
        }

        for (j = 0; j < g->nodes_num; ++j) {
                broken = broken ||
                         !g->nodes[j].check_res;
        }

        return !broken;
}

int
main(int argc,
     char **argv)
{
        graph g = { 0 };
        char broken = 0;
        char printing_first_elt = 0;
        char printing_first_base = 0;
        edge *e = 0;
        node *t = 0;
        size_t j = 0;
        size_t num_edge_nodes = 0;

        setlocale(LC_ALL, "");

        if (argc != 2) {
                printf("Usage: %s INPUT_FILE\n", argv[0]);

                return 1;
        }

        if (read_file(argv[1], &g) < 0) {
                return 1;
        }

        /* Tidy up the edges */
        for (j = 0; j < g.edges_num; ++j) {
                e = &g.edges[j];

                if (!e->r) {
                        t = e->l;
                        e->l = e->r;
                        e->r = t;
                } else if (e->l &&
                           e->r &&
                           strcmp(e->l->name, e->r->name) > 0) {
                        t = e->l;
                        e->l = e->r;
                        e->r = t;
                }
        }

        qsort(g.edges, g.edges_num, sizeof *g.edges, comp_edge);

        /*
         * Now all the border nodes (the 0s) are at the front - how
         * many are there? While we're here, fix the boundary edges as
         * LEFT at initial depth 0.
         */
        for (j = 0; j < g.edges_num; ++j) {
                e = &g.edges[j];

                if (!e->l) {
                        num_edge_nodes++;
                        e->fix_depth = 0;
                        e->d = D_LEFT;
                } else {
                        e->fix_depth = (unsigned int) -1;
                }
        }

        /*
         * Count upwards through the possible start source/sink
         * configurations, in binary
         */
        printf("{\n");
        printing_first_base = 1;
        do {
                for (j = num_edge_nodes; j < g.edges_num; ++j) {
                        g.edges[j].fix_depth = (unsigned int) -1;
                }

                broken = 0;
                update_to_fixed_point(&g, 0, &broken, 1);

                if (!broken &&
                    admits_perfect_orientation(&g, 1)) {
                        if (!printing_first_base) {
                                printf(",\n  { ");
                        } else {
                                printf("  { ");
                                printing_first_base = 0;
                        }
                        printing_first_elt = 1;

                        for (j = 0; j < num_edge_nodes; ++j) {
                                if (g.edges[j].d == D_LEFT) {
                                        if (!printing_first_elt) {
                                                printf(", ");
                                        }
                                        printf("%s", NODE_NAME(g.edges[j].r));
                                        printing_first_elt = 0;
                                }
                        }

                        printf(" }");
                }

                if (g.edges[0].d == D_LEFT) {
                        g.edges[0].d = D_RIGHT;
                } else {
                        j = 0;

                        while (j < num_edge_nodes) {
                                if (g.edges[j].d == D_RIGHT) {
                                        g.edges[j].d = D_LEFT;
                                } else {
                                        g.edges[j].d = D_RIGHT;
                                        break;
                                }

                                j++;
                        }

                        if (j >= num_edge_nodes) {
                                /* Must have tried all configurations */
                                break;
                        }
                }
        } while (1);
        printf("\n}\n");

        free(g.edges);

        for (j = 0; j < g.nodes_num; ++j) {
                free(g.nodes[j].name);
        }

        free(g.nodes);

        return 0;
}
